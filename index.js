console.log("Hello World");

const details = {
	firstName: "French",
	lastName: "Bangcaya",
	age: "18 years old",
	hobbies: [
		"playing", "playing instruments", "sleeping"],
	workAddress: {
		housenumber: "Blk. 92 Lot 3",
		street: "Ida Street",
		city: "Bulacan",
		state: "Philippines"
		}
}
const work =
Object.values(details.workAddress);
console.log("My First Name is " + details.firstName)
console.log("My Last Name is " + details.lastName)
console.log('Yes, I am ${details.firstName} ${details.lastName}.')
console.log("I am " + details.age + " years old.")
console.log('My hobbies are ${details.hobbies.join(', ')}. ');
console.log("I work at " + work.join (" , ") + ".")